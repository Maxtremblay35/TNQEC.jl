struct Transformation{d, N}
    permutation::Vector{Int}
    transformation_noise::Vector{Float64}
    #TO DO : transformation of PS

    function Transformation{d, N}(permutation::Vector{Int}, noise_prob::Vector{Float64}) where {d, N}
        if !(sum(noise_prob) ≈ 1.0)
            error("The probability of all transformation noise must sum to 1.")
        end
        if !isperm(permutation)
            error("Invalid permutation.")
        end
        if length(permutation) != d^N
            error("The permutation must acts on d^N elements.")
        end
        if length(noise_prob) != d
            error("The length of transformation noise must be d.")
        end
        transformation_noise = cumsum(noise_prob)
        return new{d, N}(permutation, transformation_noise)
    end
end

function Transformation(permutation::Vector{Int}, noise_prob::Vector{Float64})
    d = length(noise_prob)
    N = Int(round(log(d, length(permutation))))
    return Transformation{d, N}(permutation, noise_prob)
end

function perfect_transformation(permutation::Vector{Int}, d::Int)
    noise_prob = zeros(d)
    noise_prob[1] = 1.0
    return Transformation(permutation, noise_prob)
end

const BitTransformation{N} = Transformation{2, N}
const QubitTransformation{N} = Transformation{4, N}

length(transform::Transformation{d, N}) where {d, N} = N
n_levels(transform::Transformation{d, N}) where {d, N} = d

# Transform noise

function rand_noise(transform::Transformation)
    pos = [rand() .<= transform.transformation_noise for i in 1:length(transform)]
    return Noise([minimum(findall(p)) for p in pos], d)
end

function transform(transform::Transformation{d, N}, noise::Noise{d, N}, noise_map::NoiseMap{d}) where {d, N}   
    basis = d .^ (1 : N - 1)
    string_value = 1 + dot(basis, noise.errors .- 1)
    perm_value = transform.permutation[string_value]
    factorized_noise = digits(perm_value - 1, base = d, pad = N) .+ 1
    return noise_prob(factorized_noise, rand_noise(transform), noise_map)
end


# include("transform_pps.jl")
# include("transform_tps.jl")
# include("transform_cps.jl")
# include("transform_noise.jl")
