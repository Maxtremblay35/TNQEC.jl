__precompile__(true)
module TNQEC

    using LinearAlgebra

    import 
        Base: length, getindex, ≈, ==, !=

    export 
        ProbState, BitProbState, QubitProbState,
        ProductProbState, BitProductProbState, QubitProductProbState,
        TensorProbState, BitTensorProbState, QubitTensorProbState,
        ChainProbState, BitChainProbState, QubitChainProbState,
        Noise, BitNoise, QubitNoise,
        NoiseMap, bit_noise_map, qubit_noise_map,
        Transformation, BitTransformation, QubitTransformation, perfect_transformation, transform,
        n_levels

    include("ProbStates/prob_states.jl")
    include("noise.jl")
    include("stabilizers.jl")
    include("transformations.jl")
end