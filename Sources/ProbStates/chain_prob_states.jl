struct ChainProbState{d, N} <: ProbState{d, N}
    probs::Vector{Array{Float64, 3}}
    χ::Int

    function ChainProbState{d, N}(probs::Vector{Array{Float64, 3}}, χ::Int) where {d, N}
        virtual_size = size(probs[1], 1)    
        for prob in probs
            if size(prob, 2) != d
                error("The size of all physical indices must be d.")
            end
            if size(prob, 1) != virtual_size
                error("The size of all neighbour virtual indices must be the same.")
            end
            virtual_size = size(prob, 3)
        end
        cps = new{d, N}(probs, χ)
        return truncate!(cps)
    end
end

ChainProbState(probs::Vector{Array{Float64, 3}}, χ::Int) = 
    ChainProbState{size(probs[1], 2), length(probs)}(probs, χ)

ChainProbState(prob::Array{Float64, 3}, χ::Int = 1) = ChainProbState{size(prob,3), 1}([prob], χ)

const BitChainProbstate{N} = ChainProbState{2, N}
const QubitChainProbstate{N} = ChainProbState{4, N}

getindex(cps::ChainProbState, inds...) = ChainProbState(getindex(cps.probs, inds...), cps.χ)

function truncate!(cps::ChainProbState)
    for i in 1:length(cps)
        if size(cps.probs[i], 3) > cps.χ
            cps.probs[i], cps.probs[i+1] = truncate(cps.probs[i], cps.probs[i+1], cps.χ)
        end
    end
    return cps
end

function truncate(left_prob::Array{Float64, 3}, right_prob::Array{Float64, 3}, χ::Int)
    left_size = size(left_prob)[1:2]
    right_size = size(right_prob)[2:3]
    mat_left_prob = reshape(left_prob, prod(left_size), size(left_prob, 3))
    mat_right_prob = reshape(right_prob, size(right_prob, 1), prod(right_size))
    U, S, V = svd(mat_left_prob * mat_right_prob)
    left_prob = reshape((U * Matrix(Diagonal(S)))[:, 1:χ], left_size..., χ)
    right_prob = reshape(transpose(V[:,1:χ]), χ, right_size...)
    return left_prob, right_prob
end

