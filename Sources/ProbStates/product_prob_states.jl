struct ProductProbState{d, N} <: ProbState{d, N}
    probs::Vector{Vector{Float64}}

    function ProductProbState{d, N}(probs::Vector{Vector{Float64}}) where {d, N}
        for p in probs
            if length(p) != d
                error("All units must have length d.")
            end
            if !(sum(p) ≈ 1.0)
                error("All units probs must sum to 1.")
            end
        end
        return new{d, N}(probs)
    end
end

ProductProbState(probs::Vector{Vector{Float64}}) = 
    ProductProbState{length(probs[1]), length(probs)}(probs)

ProductProbState(prob::Vector{Float64}) = ProductProbState{length(prob), 1}([prob])

const BitProductProbstate{N} = ProductProbState{2, N}
const QubitProductProbstate{N} = ProductProbState{4, N}

getindex(pps::ProductProbState, inds...) = ProductProbState(pps.probs[inds...])
