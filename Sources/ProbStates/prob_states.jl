abstract type ProbState{d, N} end

const BitProbstate{N} = ProbState{2, N}
const QubitProbstate{N} = ProbState{4, N}

length(ps::ProbState{d, N}) where {d, N} = N
n_levels(ps::ProbState{d, N}) where {d, N} = d

==(ps1::ProbState, ps2::ProbState) = ps1.probs == ps2.probs
!=(ps1::ProbState, ps2::ProbState) = ps1.probs != ps2.probs
≈(ps1::ProbState, ps2::ProbState) = ps1.probs ≈ ps2.probs

include("product_prob_states.jl")
include("tensor_prob_states.jl")
include("chain_prob_states.jl")

