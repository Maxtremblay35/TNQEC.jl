struct TensorProbState{d, N} <: ProbState{d, N}
    probs::Array{Float64, N}

    function TensorProbState{d, N}(probs::Array{Float64}) where {d, N}
        if length(unique(size(probs))) != 1 || size(probs, 1) != d
            error("All dimensions of probs must have length d.")
        end
        if !(sum(probs) ≈ 1.0)
            error("The sum of all probs must be 1.")
        end
        return new{d, N}(probs)
    end
end

TensorProbState(probs::Array{Float64, N}) where {N} = TensorProbState{size(probs, 1), N}(probs)

const BitTensorProbstate{N} = TensorProbState{2, N}
const QubitTensorProbstate{N} = TensorProbState{4, N}

function getindex(tps::TensorProbState, inds)
    dims_to_sum = Int[]
    for dim in 1:length(tps)
        if !(dim in inds)
            push!(dims_to_sum, dim)
        end
    end 
    new_probs = sum(tps.probs, dims = dims_to_sum)
    new_probs = reshape(new_probs, fill(n_levels(tps), length(inds))...)
    return TensorProbState(new_probs)
end