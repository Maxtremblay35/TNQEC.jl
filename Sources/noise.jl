struct Noise{d, N}
    errors::Vector{Int}

    function Noise{d, N}(errors::Vector{Int}) where {d, N}
        if sum(1 .<= errors .<= d) != N
            error("All errors must be between 1 and d.")
        end
        if length(errors) != N
            error("There must be N errors.")
        end
        return new{d, N}(errors)
    end
end

Noise{d}(errors::Vector{Int}) where {d} = Noise{d, length(errors)}(errors)
Noise(errors::Vector{Int}, d::Int) = Noise{d}(errors)
Noise{d}(errors::Int) where {d} = Noise{d, 1}([errors])
Noise(errors::Int, d::Int) = Noise{d}([errors])

const BitNoise{N} = Noise{2, N}
const QubitNoise{N} = Noise{4, N}

length(noise::Noise{d, N}) where {d, N} = N
n_levels(noise::Noise{d, N}) where {d, N} = d

==(noise1::Noise{d, N}, noise2::Noise{d, N}) where {d, N} = noise1.errors == noise2.errors
!=(noise1::Noise{d, N}, noise2::Noise{d, N}) where {d, N} = noise1.errors != noise2.errors

getindex(noise::Noise{d}, inds...) where {d} = Noise(getindex(noise.errors, inds...), d)

struct NoiseMap{d}
    mapping::Matrix{Int}

    function NoiseMap{d}(mapping::Matrix{Int}) where {d}
        if size(mapping) != (d, d)
            error("The mapping must be a d by d matrix.")
        end
        for i in 1:d
            if !isperm(mapping[i,:])
                error("Every row must be a permutation.")
            end
            if !isperm(mapping[:,i])
                error("Every column must be a permutation.")
            end
        end
        return new{d}(mapping)
    end
end

NoiseMap(mapping::Matrix{Int}) = NoiseMap{maximum(mapping)}(mapping)

n_levels(::NoiseMap{d}) where {d} = d

const bit_noise_map = NoiseMap{2}([1 2 ; 2 1])
const qubit_noise_map = NoiseMap{4}([1 2 3 4; 2 1 4 3 ; 3 4 1 2 ; 4 3 2 1])

==(noise_map1::NoiseMap{d}, noise_map2::NoiseMap{d}) where {d} = noise_map1.mapping == noise_map2.mapping
!=(noise_map1::NoiseMap{d}, noise_map2::NoiseMap{d}) where {d} = noise_map1.mapping != noise_map2.mapping

getindex(noise_map::NoiseMap, a::Int, b::Int) = getindex(noise_map.mapping, a, b)
getindex(noise_map::NoiseMap, a::Int) = getindex(noise_map.mapping, a, Colon())

noise_prod(noise1::Noise{d, N}, noise2::Noise{d, N}, noise_map::NoiseMap{d}) where {d, N} =
    Noise([noise_map[noise1.errors[i], noise2.errors[i]] for i in 1:N], d)

