@testset "Product Prob States" begin
    @testset "  Construction" begin        
        probs = Vector{Vector{Float64}}(undef, 10)
        for i in 1:10
            probs[i] = rand(4)
            probs[i] ./= sum(probs[i])
        end

        # Check sane case.
        pps = ProductProbState(probs)
        @test length(pps) == 10

        # Check that all units have same length.
        probs[end] = rand(3)
        probs[end] ./= sum(probs[end])
        @test_throws ErrorException ProductProbState(probs)

        # Check that all probs sum to 1.
        probs[end] = rand(4) .* 5
        @test_throws ErrorException ProductProbState(probs)
    end      

    @testset "  Getindex" begin
        pps = ProductProbState([[0.2, 0.8], [0.3, 0.7], [0.8, 0.2]])
        @test pps[1] ≈ ProductProbState([0.2, 0.8])
        @test pps[2] ≈ ProductProbState([0.3, 0.7])
        @test pps[2:3] ≈ ProductProbState([[0.3, 0.7], [0.8, 0.2]])
    end
end