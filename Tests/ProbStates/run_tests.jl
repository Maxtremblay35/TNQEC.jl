# Load TNQEC

include("../Sources/tnqec.jl")
using Main.TNQEC

# Load Test

using Test

# Run tests

include("product_prob_states.jl")
include("tensor_prob_states.jl")
include("chain_prob_states.jl")