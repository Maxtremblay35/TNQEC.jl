@testset "Tensor Prob States" begin
    @testset "  Construction" begin        
        # Check sane case.
        probs = rand(3,3,3,3)
        tps = TensorProbState(probs ./ sum(probs))
        @test length(tps) == 4

        # Check that all units have same length.
        probs = rand(3,3,2,3)
        @test_throws ErrorException TensorProbState(probs ./ sum(probs))

        # Check that all probs sum to 1.
        probs = rand(3,3,3) .* 5
        @test_throws ErrorException TensorProbState(probs)
    end      

    @testset "  Getindex" begin
        probs = zeros(2,2,2)
        probs[:,:,1] = [0.25 0.05 ; 0.1 0.2]
        probs[:,:,2] = [0.15 0.05 ; 0.1 0.1]
        
        tps = TensorProbState(probs)
        
        @test tps[1] ≈ TensorProbState(reshape(sum(probs, dims = 2:3), 2))
        @test tps[2:3] ≈ TensorProbState(reshape(sum(probs, dims = 1), 2, 2))
    end
end