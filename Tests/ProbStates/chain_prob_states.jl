@testset "Chain Prob States" begin
    @testset "  Construction" begin        
        probs = Vector{Array{Float64, 3}}(undef, 10)
        probs[1] = rand(1, 10, 5)
        for i in 2:8
            probs[i] = rand(5, 10, 5)
        end
        probs[9] = rand(5, 10, 6)
        probs[10] = rand(6, 10, 1)

        # Check sane case.

        cps = ChainProbState(probs, 20)
        @test length(cps) == 10

        # Check truncation.
        
        cps = ChainProbState(probs, 5)
        for i in 1:9
            @test size(cps.probs[i], 3) == 5
        end

        cps = ChainProbState(probs, 3)
        for i in 1:9
            @test size(cps.probs[i], 3) == 3
        end
        
        # Check that all sites has same physical size.

        probs[5] = rand(5, 12, 5)
        @test_throws ErrorException ChainProbState(probs, 10)

    end      

    @testset "  Getindex" begin
        probs = Vector{Array{Float64, 3}}(undef, 3)
        probs[1] = rand(1, 5, 10)
        probs[2] = rand(10, 5, 10)
        probs[3] = rand(10, 5, 1)

        cps = ChainProbState(probs, 5)
        @test cps[1] ≈ ChainProbState(probs[1], 5)
        @test cps[2] ≈ ChainProbState(probs[2], 5)
        @test cps[2:3] ≈ ChainProbState(probs[2:3], 5)
    end

    @testset "  Truncate" begin
        
    end
end