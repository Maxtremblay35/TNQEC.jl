@testset "Noise" begin
    @testset "  Construction" begin
        # Check that everything is between 1 and d.
        @test_throws ErrorException Noise([1,2,3,2,3,4], 3)
        @test_throws ErrorException Noise([10, 9, 3, 4, 5, 11, 13, 10, 5, 7, 12], 10)

        noise = BitNoise([1, 2, 1, 1, 1, 1, 2])
        @test length(noise) == 7
        @test n_levels(noise) == 2
    end

    @testset " getindex" begin
        noise = QubitNoise([1,2,4,1,1])
        @test noise[1:2] == QubitNoise([1, 2])
        @test noise[3] == QubitNoise(4)
        @test noise[5:-1:3] == QubitNoise([1, 1, 4])
    end
end

@testset "Noise Map" begin
    @testset "  Construction" begin
        # Test that it is a valid permutation
        mapping = [1 2 3 ; 1 2 3 ; 1 2 3]
        @test_throws ErrorException NoiseMap(mapping)
        
        # Test for the size
        mapping = [1 2 ; 2 3 ; 3 1]
        @test_throws ErrorException NoiseMap(mapping)

        mapping = [1 2 3 ; 2 3 1 ; 3 1 2]
        noise_map = NoiseMap(mapping)
        @test n_levels(noise_map) == 3
    end

    @testset "  getindex" begin
        mapping = [1 2 3 ; 2 3 1 ; 3 1 2]
        noise_map = NoiseMap(mapping)
        @test noise_map[1,2] == 2
        @test noise_map[3,3] == 2
        @test noise_map[2] == [2, 3, 1]
    end
end
