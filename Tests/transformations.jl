@testset "Transformation" begin
    @testset "  Construction" begin
        # Check prob sum to 1
        @test_throws ErrorException Transformation([1, 4, 3, 2], [0.3, 0.3, 0.3, 0.3])

        # Check that the permutation is valid
        @test_throws ErrorException Transformation([1, 4, 1, 4], [0.8, 0.1, 0.0, 0.1])

        # Check that the length are good.
        @test_throws ErrorException Transformation([1, 4, 3, 2, 5], [0.2, 0.8])

        # Check good case
        transformation = Transformation([1, 4, 3, 2], [0.9, 0.1])
        @test length(transformation) == 2
        @test n_levels(transformation) == 2
        @test transformation.transformation_noise ≈ [0.9, 1.0]
    end

    @testset "Transform noise" begin
        # TO DO !!!      
    end
end

