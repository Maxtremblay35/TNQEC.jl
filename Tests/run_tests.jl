# Load TNQEC

include("../Sources/tnqec.jl")
using Main.TNQEC

# Load Test

using Test

# Run tests

include("noise.jl")

@testset "Prob States" begin
    include("ProbStates/product_prob_states.jl")
    include("ProbStates/tensor_prob_states.jl")
    include("ProbStates/chain_prob_states.jl")
end

include("transformations.jl")
include("stabilizers.jl")
